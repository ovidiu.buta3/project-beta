import React from "react";

class AutomobilesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            autos_records: []
        };
    }
   

  //endpoints
  static salesRecordUrl = 'http://localhost:8100/api/automobiles/';
  

  async componentDidMount() {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (response.ok) {
          const data = await response.json()
          this.setState({ autos_records: data.autos });
      }
  }
    

render() {
    const image_width = {
        width: '50%'
      };
    return (
        <>
        <h1>Automobile List</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Model</th>
            <th>Color</th>
            <th>Year</th>
            <th>Picture</th>
            <th>Vin</th>
        </tr>
        </thead>
        <tbody>
        {this.state.autos_records ? this.state.autos_records.map(_auto => {
            return (
                <tr key={_auto.id}>
                        <td>{_auto.model.name}</td>
                        <td>{_auto.color}</td>
                        <td>{_auto.year}</td>
                        <td>{<img style={image_width} src={_auto.model.picture_url}/>}</td>
                        <td>{_auto.vin}</td>
                        
                </tr>
                    );
        }):null}
        </tbody>
    </table>
        </>
    );
}
}

export default AutomobilesList;
