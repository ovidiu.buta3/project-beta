import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewCustomerForm from './CreateCustomer';
import NewSalesRepForm from './CreateSalesRep';
import AutomobileForm from './CreateAutomobile';
import ManufacturerForm from './CreateManufacturer';
import CreateSalesRecord from './CreateSales';
import SalesList from './SalesList';
import ManufacturerList from './ManufacturersHistory';
import SalesByRep from './SalesRepHistory';
import ModelForm from './CreateModel';
import ModelList from './ModelsHistory';
import AutomobilesList from './AutomobilesList';
import ApptList from './ApptList';
import TechForm from './TechnicianForm';
import ServiceHistoryVin from './ServiceHistory';
import ServiceForm from './ServiceForm';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/ApptList/" element={<ApptList />} />
          <Route path="/ApptList/new" element={<ServiceForm />} />
          <Route path="/ApptList/ServiceHistory/" element={<ServiceHistoryVin />} />
          <Route path="/techs/new/" element={<TechForm />} />
          <Route path="customers">
            <Route path="new" element={<NewCustomerForm />} />
          </Route>
          <Route path="salesreps">
            <Route path="new" element={<NewSalesRepForm />} />
          </Route>
          <Route path="salesrecord">
            <Route path="new" element={<CreateSalesRecord />} />
          </Route>
          <Route path="salesrecord">
            <Route path="list" element={<SalesList />} />
          </Route>
          <Route path="salesreps">
            <Route path="history" element={<SalesByRep />} />
          </Route>
          <Route path="automobile">
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="automobile">
            <Route path="list" element={<AutomobilesList />} />
          </Route>
          <Route path="model">
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="model">
            <Route path="list" element={<ModelList />} />
          </Route>
          <Route path="manufacturer">
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="manufacturer">
            <Route path="list" element={<ManufacturerList />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
