import React from 'react';

class NewSalesRepForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employeeNumber: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    static employeeUrl = 'http://localhost:8090/api/salesreps/'

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.employee_number = data.employeeNumber;
        delete data.employeeNumber;


        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch("http://localhost:8090/api/salesreps/", fetchConfig)
        if (response.ok) {
            const newSalesRep = await response.json()
            console.log(newSalesRep)
            //cleaning fields..to auto delete text
            const empty_obj = {
                name: '',
                employeeNumber: '',
            };
            this.setState(empty_obj);
        }
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sales Person</h1>
                        <form onSubmit={this.handleSubmit} id="create-new-sales-rep-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Employee Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.employeeNumber} placeholder="Employee number" required type="text" name="employeeNumber" id="employee_number" className="form-control" />
                                <label htmlFor="employee_number">Employee ID</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default NewSalesRepForm;
