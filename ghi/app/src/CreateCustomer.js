import React from 'react';

class NewCustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            phoneNumber: '',
            address: '',
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // endpointurile
    static customerUrl = 'http://localhost:8090/api/customers/'

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.phone_number = data.phoneNumber;
        delete data.phoneNumber;

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch("http://localhost:8090/api/customers/", fetchConfig)
        if (response.ok) {
            const newCustomer = await response.json()
            console.log(newCustomer)
            // clean form
            const empty_obj = {
                name: '',
                address: '',
                phoneNumber: '',
            };
            this.setState(empty_obj);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({phoneNumber: value})
    }

    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({address: value})
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-new-customer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name"
                                    id="name" className="form-control" />
                                <label htmlFor="name">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePhoneNumberChange} value={this.state.phoneNumber} placeholder="Phone number" required type="text" name="phone_number"
                                    id="phone_number" className="form-control" />
                                <label htmlFor="phone_number">Phone Number(with country code)</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleAddressChange} value={this.state.address} placeholder="Address" required type="text" name="address"
                                    id="address" className="form-control" />
                                <label htmlFor="address">Customer Address</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default NewCustomerForm;
