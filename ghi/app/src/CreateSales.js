import React from 'react';

class CreateSalesRecord extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            salesReps: [],
            customers: [],
            automobiles: [],
            price: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state} ;
        delete data.salesReps;
        delete data.customers;
        delete data.automobiles;
        console.log("handle submit", data)

        const salesRecordUrl = 'http://localhost:8090/api/salesrecord/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch('http://localhost:8090/api/salesrecord/', fetchConfig)
        if (response.ok) {
            const newSalesRecord = await response.json()

            window.location.reload()

            const cleared = {
                price: "",
                salesRep: "",
                customer: "",
                automobile: "",
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const customerUrl = 'http://localhost:8090/api/customers/';
        const repUrl = 'http://localhost:8090/api/salesreps/';
        const salesRecordsUrl = 'http://localhost:8090/api/salesrecord/';

        const autoResponse = await fetch(autoUrl);
        const customerResponse = await fetch(customerUrl);
        const repResponse = await fetch(repUrl);
        const salesRecordsResponse = await fetch(salesRecordsUrl)

        if (autoResponse.ok && customerResponse.ok && repResponse.ok && salesRecordsResponse.ok) {
        const autoData = await autoResponse.json();
        const customerData = await customerResponse.json();
        const repData = await repResponse.json();
        const salesRecordsData = await salesRecordsResponse.json();


        this.setState({ automobiles: autoData.autos });
        this.setState({ customers: customerData.customers });
        this.setState({ salesReps: repData.sales_reps });

        const auto_vin_only = []

        for (let auto of autoData.autos){
            auto_vin_only.push(auto.vin)
        }

        const sales_records_vins = []

        for (let sale of salesRecordsData.all_sales_records){
            sales_records_vins.push(sale.automobile.vin)
        }


        const unsold_autos_lst = []
        for (let vin of auto_vin_only){
            if (!sales_records_vins.includes(vin)){
                unsold_autos_lst.push(vin)
            }
        }

        const result_list = []
        for (let auto of autoData.autos){
            if (unsold_autos_lst.includes(auto.vin)){
                result_list.push(auto)
            }
        }

        this.setState({ automobiles: result_list });
        }
    }



    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sale</h1>
                        <form onSubmit={this.handleSubmit} id="create-new-sales-record-form">

                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                                    <option value="">Select an automobile</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>
                                                {automobile.vin}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.salesRep} required name="sales_rep" id="sales_reps" className="form-select">
                                    <option value="">Select a sales rep</option>
                                    {this.state.salesReps.map(salesRep => {
                                        return <option key={salesRep.id} value={salesRep.id}>{salesRep.name}</option>
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                                    <option value="">Select a customer</option>
                                    {this.state.customers.map(customer => {
                                        return <option key={customer.id} value={customer.id}>{customer.name}</option>
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                                <label htmlFor="price">Car Cost</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateSalesRecord;
