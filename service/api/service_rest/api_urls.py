from django.urls import path

from .api_views import (api_list_appointments, api_appointment_details, api_list_technicians, api_technician_details, api_list_ServiceHistory)

urlpatterns = [
    path("ApptList/", api_list_appointments, name="api_list_appointments"),
    path("techs/", api_list_technicians, name="api_list_technicians"),
    path("ApptList/<int:pk>/", api_appointment_details, name="api_show_appt"),
    path("ServiceHistory/<str:vin>/", api_list_ServiceHistory, name="api_list_ServiceHistory"),
    path("techs/<int:pk>/", api_technician_details, name="api_show_tech"),
]
